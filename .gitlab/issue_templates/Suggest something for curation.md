## Link to article for curation

<!--
Links should be ALWAYS in HTTPS, preferrly an archive URL via Internet Archive or other web archiving service.
-->

## Summary in 2-4 sentences

<!--
We only speak English, so summaries should be in American or British English. The editorial team has the right
to modify your summary to ensure it upholds our editorial policies, ensure it doesn't promote spam, racism, and illegal
content, and there's no PII in it..
-->

## Any content warnings, if applicable?

<!--
Used by the newsletter team to place potentially senstive content under an landing page
to avoid issues on reader's side.
-->

## Category

Please tick one or more categories below for this article by replacing whitespace with X inside the brackets.

* [ ] Technology
* [ ] Open-Source
* [ ] Specific to an fandom (tick the suboptions below)
  * [ ] Minecraft SMPs (please specify):
  * [ ] Other gaming community (please specify):
  * [ ] Others (please specify):
* [ ] Community-related (please specify):
* [ ] Internet Shitfuckery (this isn't TheJuiceMedia, please don't send us satire suggestions)
* [ ] Others (please specify):

<!-- DO NOT REMOVE STUFF BELOW THIS LINE! -->
/label ~"newsletter::suggestions-for-curation" ~"project::community-radar" ~"severity::wishlist"
