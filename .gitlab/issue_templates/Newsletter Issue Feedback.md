<!--

Hi there! Please fill up the template below to assit the newsletter team for your
feedback to be processed and applied. Valid critism is accepted, through any hate mail
without following the code of conduct will make your issue closed and locked without
notification.

-->

## Provide us the link to the newsletter issue from Tumblr/Substack for cross-reference

<!--

We just need one link to that newsletter issue from either Tumblr, Substack or lists.sr.ht, so that
we can fix the problem. Once you got the link, please remove the examples below and replace it with
the link you copied.

-->

<!-- SUBSTACK -->
https://communityradar.substack.com/p

<!-- TUMBLR -->
https://community-radar-recaptime.tumblr.com/

<!-- LISTS.SR.HT -->
https://lists.sr.ht/~recaptime-dev/community-radar/


## Your feedback below this header

<!--

Additional context through screenshots are welcome here, through off-topic text maybe editted out
or the issue will be vanished in its entirity.

-->

<!-- DO NOT REMOVE STUFF BELOW THIS LINE! -->
/label ~"newsletter::feedback" ~"project::community-radar" ~"severity::normal"
