# Newsletter FAQ

We keep an list of frequently asked questions aboutthe newsletter, so we don't repeat answering it over and over again. If you have any questions even afer reading this, please [create an new issue][ask-questions] so we can answer it. 

## Content license?

Text under CC BY-SA 4.0, excluding content linked and embedded media unless otherwise noted.

## What topics Community Radar usually covers in each issue?

Right now, we're covering mostly tech and open-source news and then some, including community news from tech and OSS communities as well as some Minecraft communities.

If you want to help us cover more stuff as contributor, feel free to [submit one here][submit-for-curation].

## Does you include controversal content or articles that discusses/mentions controversal content or stuff that subject to Big Tech's censorship?

We try our best to skip it or atleast give readers minimal context.

[submit-for-curation]: https://gitlab.com/RecapTime/community-radar/issues/new?issuable_template=Suggest%20something%20for%20curation
[ask-questions]: https://gitlab.com/RecapTime/community-radar/issues/new?issuable_template=Ask%20an%20question