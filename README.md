# Community Radar Newsletter - Issue Tracker

[![Substack](https://img.shields.io/badge/Subscribe%20on-Substack-orange?logo=substack&style=for-the-badge)](https://communityradar.substack.com)
[![Sourcehut](https://img.shields.io/badge/Subscribe%20on-Sourcehut-black?style=for-the-badge)](https://lists.sr.ht/~recaptime-dev/community-radar)

This GitLab project's purposes are to:

* track user feedback about the content of every issue we publish on monthly/quarterly basis,
* submit content suggestions for curation in future issues, and
* report possible issues found in existing issues for editors to be fixed, such as content warnings

**Looking for the newsletter archives themselves in Markdown?** Our good old [newsletter archives are still there](https://gitlab.com/RecapTime/newsletter-archive) for your needs.

Please be reminded that this issue tracker isn't the right place for anything related to copyright issues in the newsletter issues or sponsorship requests. Please see the [`docs/faq.md`](./docs/faq.md) for details.

If you're viewing this README from Sourcehut at the project hub, this is where it holds everything about Community Radar and other newsletters we do in one place on Sourcehut.

## Editors/Maintainers

| GitLab Username | Email | Socials | Role |
| --- | --- | --- | --- |
| @ajhalili2006 | <andreijiroh@recaptime.tk>, <ajhalili2006@gmail.com> | <https://ajhalili2006.bio.link> | Recap Time Squad member, lead editor |
